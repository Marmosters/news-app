//
//  ArticleModel.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit


struct Articles: Codable {
    let articles: [ArticleModel]
    let totalResults: Int
}

struct ArticleModel: Codable {
    var source: ArticleSourceModel?
    var author: String?
    var title: String?
    var description: String?
    var url: String?
    var urlToImage: String?
    var publishedAt: String?
    var content: String?
    var image: UIImage?
    
    enum CodingKeys: String, CodingKey {
        case source
        case author
        case title
        case description
        case url
        case urlToImage
        case publishedAt
        case content
    }
}
