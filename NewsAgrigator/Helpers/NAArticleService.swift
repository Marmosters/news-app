//
//  NAArticleService.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/31/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class NAArticleService: NANetwokkCommunicationProtocol {
    var endpoint = NewsAPI.topHeadlinesEndpoint
    var params: [String: Any]?
    
    let urlSsession: URLSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var totalResults: Int?
    var page = 1

    func fetchArticles(country: String, completion: (([ArticleModel]?, String?) -> Void)?) {
        params = ["page": page,
                  "country": country]
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.fetchNetworkData {  data, error in
                self?.totalResults = 0
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        completion?(nil, error)
                    }
                    return
                }
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(Articles.self, from: data)
                    DispatchQueue.main.async {
                        self?.totalResults = result.totalResults
                        completion?(result.articles, nil)
                    }
                    
                } catch {
                    DispatchQueue.main.async {
                        completion?(nil, error.localizedDescription)
                    }
                }
            }
        }
    }
}
