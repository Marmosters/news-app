//
//  ArticleSourceMO+CoreDataProperties.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/13/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//
//

import Foundation
import CoreData


extension ArticleSourceMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ArticleSourceMO> {
        return NSFetchRequest<ArticleSourceMO>(entityName: "ArticleSource")
    }

    @NSManaged public var id: Int32
    @NSManaged public var sourceId: String?
    @NSManaged public var name: String?
    @NSManaged public var url: String?
    @NSManaged public var category: String?
    @NSManaged public var language: String?
    @NSManaged public var country: String?
    @NSManaged public var logo: Data?
    
    func initWithSource(_ source: ArticleSourceModel) {
        self.sourceId = source.id
        self.name = source.name
        self.url = source.url
        self.category = source.category
        self.language = source.language
        self.country = source.country
    }
    
    var sourceObject: ArticleSourceModel {
        var model = ArticleSourceModel()
        model.id =  self.sourceId
        model.name = self.name
        model.url = self.url
        model.category = self.category
        model.language = self.language
        model.country = self.country
        return model
    }
}
