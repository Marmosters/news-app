//
//  ArticlesViewController.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class ArticlesViewController: BaseViewController, ArticlesPresnterDelegate {

    @IBOutlet weak var tableView: UITableView!
    
    private let presenter = ArticlesPresenter()
    private let refreshControl = UIRefreshControl()
    private var settingsButton: UIBarButtonItem!
    private weak var spinner: UIActivityIndicatorView?
    var isOnline: Bool = true {
        didSet {
            settingsButton.isEnabled = isOnline
            if isOnline && isOnline != oldValue {
                tableView.reloadData()
            }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NAStrings.topHeaderTitle
        configureTableView()
        setSettingsButton()
        presenter.delegate = self
        presenter.isAppConfigured ? tableView.reloadData() : performSegue(withIdentifier: NAConstants.SequeIdentifers.contrySelectionSegue, sender: nil)
    }
    
    private func configureTableView() {
        tableView.register(UINib(nibName: NAConstants.NibNames.articleCell, bundle: nil), forCellReuseIdentifier: NAConstants.ReuseIdentifers.atricleCellIdentifer)
        tableView.register(UINib(nibName: NAConstants.NibNames.loadingCell, bundle: nil), forCellReuseIdentifier: NAConstants.ReuseIdentifers.loadingCellIdentifer)
        tableView.rowHeight = 50.0
        tableView.estimatedRowHeight = UITableView.automaticDimension
        
        refreshControl.addTarget(self, action: #selector(reloadArticles(sender:)), for: .valueChanged)
        if #available(iOS 10.0, *) {
            tableView.refreshControl = refreshControl
        } else {
            tableView.addSubview(refreshControl)
        }
    }
    
    private func setSettingsButton() {
        settingsButton = UIBarButtonItem(title: "⚙︎", style: .plain, target: self, action: #selector(settingsButtonTapped(sender:)))
        if let font = UIFont(name: "Helvetica", size: 30.0) {
            settingsButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .normal)
            settingsButton.setTitleTextAttributes([NSAttributedString.Key.font: font], for: .disabled)
        }
        navigationItem.rightBarButtonItem = settingsButton
    }
    
    @objc private func settingsButtonTapped(sender: UIBarButtonItem) {
        performSegue(withIdentifier: NAConstants.SequeIdentifers.contrySelectionSegue, sender: nil)
    }
    
    private func fetchArticles() {
        spinner?.startAnimating()
        presenter.fetchArticles { [weak self] error in
            self?.spinner?.stopAnimating()
            self?.refreshControl.endRefreshing()
            self?.tableView.reloadData()
            if let error = error {
                let alet = UIAlertController(title: NAStrings.errorTitle, message: error, preferredStyle: .alert)
                alet.addAction(UIAlertAction(title: NAStrings.actionOK, style: .default, handler: nil))
                self?.present(alet, animated: true)
            }
        }
    }
        
    @objc private func reloadArticles(sender: Any? = nil) {
        presenter.resetArticles(refresh: sender is UIRefreshControl)
        tableView.reloadData()
        if sender is UIRefreshControl {
            fetchArticles()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let nextVC = segue.destination as? CountriesViewController {
            nextVC.delegate = self
            nextVC.navigationItem.setHidesBackButton(!presenter.isAppConfigured, animated: false)
        }
        if let nextVC = segue.destination as? ArticleDetailsViewController, let model = sender as? ArticleModel {
            nextVC.article = model
        }
    }

}

extension ArticlesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter.numberOfArtincles + (presenter.hasMoreData ? 1 : 0)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == presenter.numberOfArtincles && presenter.hasMoreData {
            let cell = tableView.dequeueReusableCell(withIdentifier: NAConstants.ReuseIdentifers.loadingCellIdentifer) as? LoadingTableViewCell ?? LoadingTableViewCell()
            spinner = cell.spinner
            fetchArticles()
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: NAConstants.ReuseIdentifers.atricleCellIdentifer, for: indexPath) as? ArticleTableViewCell ?? ArticleTableViewCell()
        cell.titleLabel.numberOfLines = 0
        cell.sourceLabel.numberOfLines = 0
        cell.initWithArtice(presenter.articleForRowAt(indexPath))
        cell.titleLabel.sizeToFit()
        cell.sourceLabel.sizeToFit()
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = presenter.articleForRowAt(indexPath)
        performSegue(withIdentifier: NAConstants.SequeIdentifers.articleDetailsSegue, sender: model)
    }
}

extension ArticlesViewController: CountrySelectionDelegte {
    func countryDidUpdated() {
        reloadArticles()
    }
}
