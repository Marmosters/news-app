Requirements:

* Reads JSON from a publicly available REST API endpoint [News API](https://newsapi.org)
* Implement ability to change country for news in collection view. If country was not selected - navigate to country selection screen
* Parses it and shows the contents in a table view,  or collection view
* Tapping on a table view item shows a detailed view of that item
* Persists country choice and contents of the JSON data locally
* If there is no internet it will show previously downloaded content

Installation: 
* Have CocoaPods installed
* Clone this repository: **git clone https://gitlab.com/Marmosters/news-app.git**
* Navigate to app folder
* Install pods: **pod install** 

