//
//  CountriesCollectionViewController.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

protocol CountrySelectionDelegte: class {
    func countryDidUpdated()
}

class CountriesViewController: BaseViewController {
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let presenter = CoutriesPresenter()
    weak var delegate: CountrySelectionDelegte!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NAStrings.countrySelectionTitle
        collectionView.register(UINib(nibName: NAConstants.NibNames.countryCell, bundle: nil), forCellWithReuseIdentifier: NAConstants.ReuseIdentifers.countryCellIdentifer)
        let width = (collectionView.frame.size.width - 50) / 2
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
          layout.itemSize = CGSize(width: width, height: width)
        }
    }
}

extension CountriesViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfCountries
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let identifer = NAConstants.ReuseIdentifers.countryCellIdentifer
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifer, for: indexPath) as? CountryCollectionViewCell ?? CountryCollectionViewCell()
        cell.initWithModel(presenter.modelForRowAt(indexPath))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter.saveCountry(atIndexPath: indexPath)
        delegate.countryDidUpdated()
        navigationController?.popViewController(animated: true)
    }
}
