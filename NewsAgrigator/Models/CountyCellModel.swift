//
//  CountyCellModel.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/7/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

struct CountryCellModel {
    let code: String
    let name: String
    let flag: String
    
    init(code: String) {
        self.code = code
        self.name = Countries.countryName(countryCode: code) ?? ""
        self.flag = Countries.emojiFlag(countryCode: code)
    }
}
