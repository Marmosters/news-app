//
//  ArticleDetailsViewController.swift
//  NewsAgrigator
//
//  Created by Vladimir on 15.11.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class ArticleDetailsViewController: BaseViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var articleImage: UIImageView!
    @IBOutlet weak var articleText: UILabel!
    
    @IBAction func readMoreButtonPressed(_ sender: UIButton) {
        if let articleURL = article.url, let url = URL(string: articleURL) {
            openUrlInSafari(url)
        }
    }
    
    var article: ArticleModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Details"
        titleLabel.text = article.title
        if let image = article.image {
            articleImage.image = image
        } else {
            articleImage.removeFromSuperview()
        }
                
        articleText.text = article.content

    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
    }

}
