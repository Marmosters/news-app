//
//  ArticlesManager.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/13/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit
import CoreData
import Reachability

class ArticlesManager: ReachabilityObserver {
    
    private let articlesNetworkService = NAArticleService()
    private let sourcesNetworkService = NASourcesService()
    private var sources = [ArticleSourceModel]()
    private var curPage: Int {
        get {
            return articlesNetworkService.page
        }
        set {
            articlesNetworkService.page = newValue
            if newValue == 1 && isConnectionReachible {
                deleteArticles()
            }
        }
    }
    
    private var isSourceUpdtaeRequired: Bool {
        guard let intervalStr: String = Settings.vlueForKey(.lastSourcesUpdate), let interval = Double(intervalStr) else { return true }
        let secondsInDay = Int(truncating: NSDecimalNumber(decimal: pow(60, 3)))
        let timeSpampAgeInDays =  Int(Date().timeIntervalSince1970 - interval) / secondsInDay
        return timeSpampAgeInDays > 0
    }
    
    
    private var context: NSManagedObjectContext? {
        return (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer.viewContext
    }
    
    
    var totalAtricles: Int? {
        get {
            return articlesNetworkService.totalResults
        }
        set {
            articlesNetworkService.totalResults = newValue
        }
    }
    
    var countryCode: String? {
        let code: String? = Settings.vlueForKey(.country)
        return code
    }
    
    func fetchArticles(completion: (([ArticleModel]?, String?) -> Void)?) {
        
        if isConnectionReachible {
            fetchArticlesFromSite { [weak self] articles, error in
                if let articlleList = articles, !articlleList.isEmpty {
                    self?.curPage += 1
                }
                DispatchQueue.main.async {
                self?.reachabilityActionDelegate?.reachabilityChanged(true)
                    completion?(articles, error)
                }
            }
        } else {
            reachabilityActionDelegate?.reachabilityChanged(false)
            fetchArticlesFromDB { artcles, error in
                    completion?(artcles, error)
            }
        }   
    }
        
    private func fetchArticlesFromSite(completion: (([ArticleModel]?, String?) -> Void)?) {
        guard let code = countryCode else {return}
        var errors: String = ""
        var loadedArticles: [ArticleModel]?
        DispatchQueue.global().async {
            let group = DispatchGroup()
            if self.isSourceUpdtaeRequired {
                group.enter()
                self.updateSources { error in
                    if let error = error {
                        errors += "\(error) "
                    }
                    group.leave()
                }
            } else {
                self.loadSources()
            }
            group.enter()
            self.articlesNetworkService.fetchArticles(country: code) {articles, error in
                if let error = error {
                    errors += "\(error) "
                }
                loadedArticles = articles
                group.leave()
            }
            group.wait()
            guard var articles = loadedArticles else {
                completion?(nil, errors)
                return
            }
            for i in articles.indices {
                if let sourceId = articles[i].source?.name, let source = self.sources.first(where: {$0.name == sourceId}) {
                    articles[i].source = source
                }
                
                if let urlToImage = articles[i].urlToImage {
                    group.enter()
                    self.articlesNetworkService.downloadImage(url: urlToImage) { image in
                        articles[i].image = image
                        group.leave()
                    }
                }
            }
            group.wait()
            DispatchQueue.main.async {
                self.saveArticles(articles)
                completion?(articles, errors.isEmpty ? nil : errors)
            }
        }
    }
    
    private func fetchArticlesFromDB(completion: (([ArticleModel]?, String?) -> Void)?) {
        DispatchQueue.main.async {
            guard let articlesMO = try? self.context?.fetch(ArticlesMO.fetchRequest() as NSFetchRequest<ArticlesMO>) else {return}
            let articles = articlesMO.map({$0.articleObject})
            self.totalAtricles = articles.count
            completion?(articles, nil)
        }
    }
    
    func saveArticles(_ articles: [ArticleModel]) {
        guard let context = self.context else {return}
        for article in articles {
            let articleMO = ArticlesMO(context: context)
            articleMO.initWithAtricle(article)
            let sourceMo = ArticleSourceMO(context: context)
            if let source = article.source {
                sourceMo.initWithSource(source)
            }
            articleMO.source = sourceMo
            context.insert(articleMO)
        }

        do {
            try context.save()
        } catch {
            print(error)
        }
    }
    
    func resetData(toDefaults: Bool) {
        curPage = 1
        totalAtricles = toDefaults ? 0 : nil
    }
    
    private func updateSources(completion: ((String?) -> Void)?) {
        sourcesNetworkService.fetchSources { [weak self] sources, error in
            if let sources = sources {
                self?.sources = sources
                self?.saveSources()
            }
            completion?(error)
        }
    }
    
    private func deleteArticles() {
        DispatchQueue.main.async {
            let fetchRequest = ArticlesMO.fetchRequest() as NSFetchRequest<ArticlesMO>
            guard let request = fetchRequest as? NSFetchRequest<NSFetchRequestResult> else {return}
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            _ = try? self.context?.execute(deleteRequest)
        }
        
    }
        
    private func saveSources() {
        DispatchQueue.main.async {
            guard let context = self.context else {return}
            let fetchRequest = ArticleSourceMO.fetchRequest() as NSFetchRequest<ArticleSourceMO>
            guard let request = fetchRequest as? NSFetchRequest<NSFetchRequestResult>  else {return}
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: request)
            do {
                try context.execute(deleteRequest)
                for source in self.sources {
                    let sourceMO = ArticleSourceMO(context: context)
                    sourceMO.initWithSource(source)
                    context.insert(sourceMO)
                }
                try context.save()
            } catch {
                print(error.localizedDescription)
            }
        }

        Settings.saveValueForkey(.lastSourcesUpdate, value: Date().timeIntervalSince1970.description)
        
    }
    
    private func loadSources() {
        DispatchQueue.main.async {
            guard let sourcesMO = try? self.context?.fetch(ArticleSourceMO.fetchRequest() as NSFetchRequest<ArticleSourceMO>) else { return }
            self.sources = sourcesMO.map({$0.sourceObject})
        }

    }
    
}
