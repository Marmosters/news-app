//
//  Constsnts.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/31/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class NAConstants {
    static let newaApiLink = "https://newsapi.org"
    class ReuseIdentifers {
        static let atricleCellIdentifer = "articleCell"
        static let countryCellIdentifer = "countryCell"
        static let loadingCellIdentifer = "loadingCell"
    }
    
    class SequeIdentifers {
        static let contrySelectionSegue = "contrySelectionSegue"
        static let articleDetailsSegue = "articleDetailsSegue"
    }
    
    class NibNames {
        static let articleCell = "ArticleTableViewCell"
        static let countryCell = "CountryCollectionViewCell"
        static let loadingCell = "LoadingTableViewCell"
    }
    
    class EntityNames {
        static let sources = "ArticleSource"
        static let articles  = "Articles"
    }
    
}
