//
//  Settings.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/7/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

enum SettingsKeys: String {
    case country = "Country"
    case lastSourcesUpdate = "lastSourcesUpdate"
}

class Settings {
    static func saveValueForkey<T>(_ key: SettingsKeys, value: T) {
        switch key {
        case .country, .lastSourcesUpdate:
            UserDefaults.standard.set(value, forKey: key.rawValue)
        }
    }
    
    static func vlueForKey<T>(_ key: SettingsKeys) -> T? {
        return UserDefaults.standard.string(forKey: key.rawValue) as? T
    }
}
