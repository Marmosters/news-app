//
//  NANavigationController.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class NANavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        title = NAStrings.countrySelectionTitle
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


}
