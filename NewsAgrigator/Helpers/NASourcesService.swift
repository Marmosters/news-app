//
//  NASourcesService.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/13/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class NASourcesService: NANetwokkCommunicationProtocol {

    let urlSsession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    var endpoint = NewsAPI.sourcesEndpoint
    var params: [String: Any]?
    
    func fetchSources(completion: (([ArticleSourceModel]?, String?) -> Void)?) {
        DispatchQueue.global(qos: .userInitiated).async {
            [weak self] in
            self?.fetchNetworkData { data, error in
                guard let data = data, error == nil else {
                    DispatchQueue.main.async {
                        completion?(nil, error)
                    }
                    return
                }
                let decoder = JSONDecoder()
                do {
                    let result = try decoder.decode(ArticleSources.self, from: data)
                    DispatchQueue.main.async {
                        completion?(result.sources, nil)
                    }
                } catch {
                    completion?(nil, error.localizedDescription)
                }
            }
        }
    }
}
