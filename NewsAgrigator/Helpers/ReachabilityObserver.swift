//
//  ReachabilityObserver.swift
//
//
//  Created by Vladimir on 24.11.2019.
//

import Foundation
import Reachability

protocol ReachabilityActionDelegate: class {
    func reachabilityChanged(_ isReachable: Bool)
}

class ReachabilityObserver {
    private var reachability: Reachability! {
        return try? Reachability()
    }
    
    var isConnectionReachible: Bool {
        return reachability.connection != .unavailable
    }

    weak var reachabilityActionDelegate: ReachabilityActionDelegate?
    
    required init () {
        addReachabilityObserver()
    }
    
    deinit {
        removeReachabilityObserver()
    }
    
    func addReachabilityObserver() {
        reachability.whenReachable = { reachability in
            self.reachabilityActionDelegate?.reachabilityChanged(true)
        }
        
        reachability.whenUnreachable = { reachability in
            self.reachabilityActionDelegate?.reachabilityChanged(false)
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func removeReachabilityObserver() {
        reachability.stopNotifier()
    }
    
}
