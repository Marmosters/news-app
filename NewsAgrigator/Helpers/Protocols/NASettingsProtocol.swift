//
//  NASettingsProtocol.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 11/6/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

protocol NASettingsProtocol: class {
    var country: String {get set}
}

extension NASettingsProtocol {
    var country: String {
        return "us"
    }
}

class NASettings: NASettingsProtocol {
    var country: String = "us"
    
    
}
