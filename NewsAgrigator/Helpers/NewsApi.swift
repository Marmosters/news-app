//
//  NewsApi.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 11/6/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class NewsAPI {
    static let apiKey = "58d6d0adbc5d4118bc2071b572872ae8" //"9943192cf2f74109b5c88d7b165c293a"
    static let beseUrl = "https://newsapi.org"
    static let apiVersion = "v2"
    static let topHeadlinesEndpoint = "top-headlines"
    static let sourcesEndpoint = "sources"
    
    static func getUrl(endpoint: String, params: [String: Any]? = nil) -> URL? {
        guard var components = URLComponents(string: "\(NewsAPI.beseUrl)/\(apiVersion)/\(endpoint)") else { return nil }
            
        components.query = "apiKey=\(NewsAPI.apiKey)"
        if let params = params, !params.isEmpty {
            components.query! += params.map({"&\($0.key)=\($0.value)"}).joined()
        }
        return components.url
    }
}
