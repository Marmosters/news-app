//
//  Country.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 11/6/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class Countries {
    // swiftlint:disable:next line_length
    static let codes = ["ae", "ar", "at", "au", "be", "bg", "br", "ca", "ch", "cn", "co", "cu", "cz", "de", "eg", "fr", "gb", "gr", "hk", "hu", "id", "ie", "il", "in", "it", "jp", "kr", "lt", "lv", "ma", "mx", "my", "ng", "nl", "no", "nz", "ph", "pl", "pt", "ro", "rs", "ru", "sa", "se", "sg", "si", "sk", "th", "tr", "tw", "ua", "us", "ve", "za"]


    static func countryName(countryCode: String) -> String? {
        let current = Locale(identifier: "en_US")
        return current.localizedString(forRegionCode: countryCode)
    }

    static func emojiFlag(countryCode: String) -> String {
        var flag = ""
        let country = countryCode.uppercased()
        for uS in country.unicodeScalars {
            flag += String(UnicodeScalar(127397 + uS.value)!)
        }
        return flag
    }
}
