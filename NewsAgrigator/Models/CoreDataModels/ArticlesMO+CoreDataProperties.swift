//
//  ArticlesMO+CoreDataProperties.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/13/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//
//

import UIKit
import CoreData


extension ArticlesMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ArticlesMO> {
        return NSFetchRequest<ArticlesMO>(entityName: "Articles")
    }

    @NSManaged public var id: NSDecimalNumber?
    @NSManaged public var author: String?
    @NSManaged public var title: String?
    @NSManaged public var url: String?
    @NSManaged public var desc: String?
    @NSManaged public var image: Data?
    @NSManaged public var published: Date?
    @NSManaged public var content: String?
    @NSManaged public var source: ArticleSourceMO?
    
    var articleObject: ArticleModel {
        var model = ArticleModel()
        model.author = author
        model.title = title
        model.description = desc
        if let image = image {
            model.image = UIImage(data: image)
        }
        model.publishedAt = published?.description
        model.content = content
        model.source = source?.sourceObject
        return model
    }
    
    func initWithAtricle(_ model: ArticleModel) {
        author = model.author
        title = model.title
        url = model.url
        desc = model.description
        image = model.image?.pngData()
        content = model.content
    }
}
