//
//  NANetwokkCommunicationProtocol.swift
//  News Agrigator
//
//  Created by Vladimir on 05.11.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

protocol NANetwokkCommunicationProtocol: class {
    var urlSsession: URLSession {get}
    var dataTask: URLSessionDataTask? {get set}
    var endpoint: String {get}
    var params: [String: Any]? {get}
    func fetchNetworkData(completion: ((Data?, String?) -> Void)?)
    func downloadImage(url: String, completion: ((UIImage?) -> Void)?)
}

extension NANetwokkCommunicationProtocol {
    func fetchNetworkData(completion: ((Data?, String?) -> Void)?) {
        dataTask?.cancel()

        guard let url = NewsAPI.getUrl(endpoint: endpoint, params: params) else { return }
        
        dataTask = urlSsession.dataTask(with: url) { [weak self] data, response, error in
            defer {
                self?.dataTask = nil
            }
            var errorMessage: String?
            if let error = error {
                errorMessage = "\(NAStrings.errorTitle): \(error.localizedDescription)"
            } else if let response = response as? HTTPURLResponse, response.statusCode != 200 {
                errorMessage = "\(NAStrings.errorTitle): \(response.statusCode) \(HTTPURLResponse.localizedString(forStatusCode: response.statusCode))"
            }
            DispatchQueue.main.async {
                completion?(data, errorMessage)
            }
            
        }
        dataTask?.resume()
    }
    
    func downloadImage(url: String, completion: ((UIImage?) -> Void)?) {
        DispatchQueue.global(qos: .default).async {
            guard let url = URL(string: url), let data = try? Data(contentsOf: url) else {
                DispatchQueue.main.async {
                    completion?(nil)
                }
                return
            }
            DispatchQueue.main.async {
                completion?(UIImage(data: data))
            }
        }
    }
}
