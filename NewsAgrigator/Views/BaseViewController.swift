//
//  BaseViewController.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit
import SafariServices

class BaseViewController: UIViewController, UITextViewDelegate {

    @IBAction func newsApiButtonPressed(_ sender: UIButton) {
        if let url = URL(string: NAConstants.newaApiLink) {
            openUrlInSafari(url)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupNewsApiLabel()
       
        
    }
    
    private func setupNewsApiLabel() {
       
        let text = NSMutableAttributedString(string: "Powered by News API")
        text.addAttribute(.link, value: "https://newsapi.org", range: NSRange(location: 0, length: text.length))
    
    }
    
    func openUrlInSafari(_ url: URL) {
        let safariVC = SFSafariViewController(url: url)
        safariVC.modalPresentationStyle = .fullScreen
        present(safariVC, animated: true)
    }
}
