//
//  LoadingTableViewCell.swift
//  NewsAgrigator
//
//  Created by Vladimir on 12.11.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class LoadingTableViewCell: UITableViewCell {
    

    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
