//
//  NAStrings.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/7/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class NAStrings {
    static let topHeaderTitle = NSLocalizedString("TopHeadlinesTitle", comment: "")
    static let countrySelectionTitle = NSLocalizedString("CountrySelectionTitle", comment: "")
    static let errorTitle = NSLocalizedString("ErrorTitle", comment: "")
    static let actionOK = NSLocalizedString("OKAction", comment: "")
    
}
