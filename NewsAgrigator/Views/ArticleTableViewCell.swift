//
//  ArticleTableViewCell.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/31/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class ArticleTableViewCell: UITableViewCell {
        

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    
    private var article: ArticleModel! {
        didSet {
            titleLabel.numberOfLines = 0
            sourceLabel.numberOfLines = 0
            titleLabel.text = article?.title
            sourceLabel.text = article?.source?.url
            titleLabel.sizeToFit()
            sourceLabel.sizeToFit()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initWithArtice(_ article: ArticleModel) {
        self.article = article
    }
    
    override func prepareForReuse() {
        article = nil
        titleLabel.text = nil
        sourceLabel.text = nil
        super.prepareForReuse()
    }

}
