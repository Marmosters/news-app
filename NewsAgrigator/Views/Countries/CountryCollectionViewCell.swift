//
//  CountryCollectionViewCell.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/7/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit

class CountryCollectionViewCell: UICollectionViewCell {
    @IBOutlet private weak var flagLabel: UILabel!
    @IBOutlet private weak var nameLabel: UILabel!
    
    private var model: CountryCellModel! {
        didSet {
            guard model != nil else {
                return
            }
            flagLabel.text = model.flag
            nameLabel.text = model.name
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func prepareForReuse() {
        model = nil
        flagLabel.text = nil
        nameLabel.text = nil
        super.prepareForReuse()
    }
    
    func initWithModel(_ model: CountryCellModel) {
        self.model = model

    }

}
