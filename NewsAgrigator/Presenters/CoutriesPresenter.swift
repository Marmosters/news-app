//
//  CoutriesPresenter.swift
//  NewsAgrigator
//
//  Created by Vladimir. Evstratov on 11/7/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

class CoutriesPresenter {
    
    var numberOfCountries: Int {
        return Countries.codes.count
    }
    
    func modelForRowAt(_ indexPath: IndexPath) -> CountryCellModel {
        guard let code = counryCodeForRowAt(indexPath: indexPath) else {return CountryCellModel(code: "Error")}
        return CountryCellModel(code: code)
    }
    
    func saveCountry(atIndexPath indexPath: IndexPath) {
        let code = counryCodeForRowAt(indexPath: indexPath)
        Settings.saveValueForkey(.country, value: code)
    }
    
    func counryCodeForRowAt(indexPath: IndexPath) -> String? {
        return Countries.codes.indices.contains(indexPath.row) ? Countries.codes[indexPath.row] : nil
    }
}
