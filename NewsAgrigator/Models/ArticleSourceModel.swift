//
//  ArticleSourceModel.swift
//  News Agrigator
//
//  Created by Vladimir on 05.11.2019.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import Foundation

struct ArticleSources: Codable {
    let sources: [ArticleSourceModel]
}

struct ArticleSourceModel: Codable {
    var id: String?
    var name: String?
    var description: String?
    var url: String?
    var category: String?
    var language: String?
    var country: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case name
        case description
        case url
        case category
        case language
        case country
    }
}
