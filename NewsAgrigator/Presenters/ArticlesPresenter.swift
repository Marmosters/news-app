//
//  ArticlesPresenter.swift
//  News Agrigator
//
//  Created by Vladimir. Evstratov on 10/30/19.
//  Copyright © 2019 Vladimir. Evstratov. All rights reserved.
//

import UIKit
import Reachability

protocol ArticlesPresnterDelegate: class {
    var isOnline: Bool {get set}
}

class ArticlesPresenter: ReachabilityActionDelegate {
    
    weak var delegate: ArticlesPresnterDelegate?
    private var articles = [ArticleModel]()
    private let manager = ArticlesManager()
    
    required init() {
        manager.reachabilityActionDelegate = self
    }
    
    var hasMoreData: Bool {
        return articles.count < manager.totalAtricles ?? 1
    }
        
    var isAppConfigured: Bool {
        return manager.countryCode != nil
    }
    
    var numberOfArtincles: Int {
         return articles.count
    }

    func articleForRowAt(_ indexPath: IndexPath) -> ArticleModel {
        return articles[indexPath.row]
    }
    
    func fetchArticles(completion: ((String?) -> Void)?) {
        let country: String? = Settings.vlueForKey(.country)
        manager.fetchArticles { [weak self] articles, error in
            if  let articles = articles {
                country == Settings.vlueForKey(.country) ? (self?.articles += articles) : (self?.articles = articles)
            }
            completion?(error)
        }
    }
    
    func resetArticles(refresh: Bool) {
        manager.resetData(toDefaults: refresh)
        articles.removeAll()
    }
    
    func reachabilityChanged(_ isReachable: Bool) {
        delegate?.isOnline = isReachable
    }
}
